#include <iostream>
#include <fstream>
#include <vector>
#include <regex>

using namespace std;

string regexBuild(string toSearch){
    string result;
    for(int pos = toSearch.find('?'); pos != toSearch.npos; pos = toSearch.find('?')){
        toSearch[pos] = '.';
    }
    for (int i = 0; i < toSearch.length(); ++i) {
        if(toSearch[i] == '*'){
            toSearch.insert(i, ".");
            ++i;
        }
    }
    if(toSearch[0] != '[')
        result = ".*" + toSearch;
    else
        result = toSearch.substr(1);
    if(toSearch[toSearch.length() - 1] != ']')
        result += ".*";
    else
        result.pop_back();
    return result;
}

int search(vector<string>results, string toFind){
    for (int i = 0; i < results.size(); ++i) {
        if(results[i] == toFind)
            return i;
    }
    return -1;
}

vector<string> call(string toSearch, string searcher[]){
    vector<string> results;
    if(toSearch.find('&') != toSearch.npos){
        vector<string>first, second;
        first = call(toSearch.substr(0, toSearch.find('&')), searcher);
        second = call(toSearch.substr(toSearch.find('&') + 1), searcher);
        for (int i = 0; i < first.size(); ++i) {
            if(search(second, first[i]) != -1)
                results.push_back(first[i]);
        }
        return results;
    }
    if(toSearch.find('+') != toSearch.npos){
        vector<string> first, second;
        first = call(toSearch.substr(0, toSearch.find('+')), searcher);
        second = call(toSearch.substr(toSearch.find('+') + 1), searcher);
        for (int i = 0; i < first.size(); ++i) {
            results.push_back(first[i]);
        }
        for (int i = 0; i < second.size(); ++i) {
            if(search(results, second[i]) == -1)
                results.push_back(second[i]);
        }
        return results;
    }
    regex r(regexBuild(toSearch));
    for (int i = 0; i < 10; ++i) {
        if(regex_match(searcher[i], r)){
            results.push_back(searcher[i]);
        }
    }

}

int main() {
    ifstream f("search.in");
    string searcher[10];
    for (int acsl = 0; acsl < 9; ++acsl) {
        getline(f, searcher[acsl], ' ');
        searcher[acsl].pop_back();
    }
    getline(f, searcher[9]);
    for (int acsl = 0; acsl < 10; ++acsl) {
        string toSearch;
        getline(f, toSearch);
        vector<string> results;
        results = call(toSearch, searcher);
        cout<<results[0];
        for (int i = 1; i < results.size(); ++i) {
            cout<<", "<<results[i];
        }
        cout<<"\n";
    }
    return 0;
}
