#include <iostream>
#include <cstdio>
#include <map>

using namespace std;

map<char, int> points;
pair<int, int> locations[41];
char letters[5];

void buildMaps(){
    points['A'] = points['E'] = 1;
    points['D'] = points['R'] = 2;
    points['B'] = points['M'] = 3;
    points['V'] = points['Y'] = 4;
    points['J'] = points['X'] = 8;
    for(int i=0; i<41; ++i)
        locations[i] = {1, 1};
    for(int i=3; i<41; i+=6)
        locations[i] = {2, 1};
    for(int i=5; i<41; i+=5)
        if(locations[i] == make_pair(1, 1))
            locations[i] = {3, 1};
    for(int i=7; i<41; i+=7)
        if(locations[i] == make_pair(1, 1))
            locations[i] = {1, 2};
    for(int i=8; i<41; i+=8)
        if(locations[i] == make_pair(1, 1))
            locations[i] = {1, 3};
}

int calculatePoints(int start, int direction){
    int wordCoeficient = 1, npoints = 0;
    for(int i = start, j = 0, step = (direction == 'V' ? 10 : 1); j < 4; i+=step, ++j){
        npoints += points[letters[j]] * locations[i].first;
        wordCoeficient *= locations[i].second;
    }
    return npoints * wordCoeficient;
}

int main()
{
    freopen("scrable.in", "r", stdin);
    buildMaps();
    scanf("%c, %c, %c, %c\n", &letters[0], &letters[1], &letters[2], &letters[3]);
    for(int acsl = 0; acsl < 5; ++acsl){
        char dir[3];
        int starts[3];
        scanf("%d, %c, %d, %c, %d, %c\n", &starts[0], &dir[0], &starts[1], &dir[1], &starts[2], &dir[2]);
        int maxPoints = 0;
        for(int i=0; i<3; ++i)
            maxPoints = max(maxPoints, calculatePoints(starts[i], dir[i]));
        cout<<maxPoints<<"\n";
    }
    return 0;
}
