#include <iostream>
#include <cstdio>

using namespace std;

int main() {
    freopen("triangles.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        double ab, bc, ac, de, ef, df;
        scanf("%lf, %lf, %lf, %lf, %lf, %lf\n", &ab, &bc, &ac, &de, &ef, &df);
        if(ab == de && bc == ef && ac == df){
            cout<<"DEF\n";
        }else if(ab == de && bc == df && ac == ef){
            cout<<"DFE\n";
        }else if(ab == ef && bc == de && ac == df){
            cout<<"EDF\n";
        }else if(ab == ef && bc == df && ac == de){
            cout<<"EFD\n";
        }else if(ab == df && bc == de && ac == ef){
            cout<<"FDE\n";
        }else if(ab == df && bc == ef && ac == de){
            cout<<"FED\n";
        }else cout<<"NONE\n";
    }
    return 0;
}
