#include <iostream>
#include <string>
#include <fstream>

using namespace std;

char encode(long long int x){
    switch(x){
        case 0: return '0';
        case 1: return '1';
        case 2: return '2';
        case 3: return '3';
        case 4: return '4';
        case 5: return '5';
        case 6: return '6';
        case 7: return '7';
        case 8: return '8';
        case 9: return '9';
        case 10: return 'A';
        case 11: return 'B';
        case 12: return 'C';
        case 13: return 'D';
        case 14: return 'E';
        case 15: return 'F';
    }
}

bool search(long long int initial, long long int previous[], int l){
    for (int i = 0; i < l; ++i) {
        if(initial == previous[i])
            return true;
    }
    return false;
}

int powerOf10(int n){
    int p = 1;
    for (int i = 0; i < n; ++i) {
        p *= 10;
    }
    return p;
}

int main()
{
    ifstream f("hexfractions.in");
    for(int acsl = 0; acsl < 10; ++acsl){
        long long int initial = 0;
        long long int result[1000];
        int k = 0;
        string firstly;
        getline(f, firstly);
        for (int i = 1; i < firstly.length(); ++i) {
            initial += firstly[i] - '0';
            initial *= 10;
        }
        initial /= 10;
        int d = firstly.length() - 1;
        long long int previous[1000];
        int l = 0;
        initial *= powerOf10(10 - d);
        for(; initial;){
            initial *= 16;
            if(search(initial, previous, l))
                break;
            result[k++] = initial / 10000000000;
            previous[l++] = initial;
            initial = initial - (initial / 10000000000) * 10000000000;
        }
        cout<<".";
        for(int i = 0; i < k && i < 10; ++i){
            cout<<encode(result[i]);
        }
        cout<<"\n";
    }
    return 0;
}
