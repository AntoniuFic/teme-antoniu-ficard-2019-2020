#include <iostream>
#include <map>
#include <cstdio>

using namespace std;

map<char, int> times;

void buildMap(){
    times['1'] = 0;
    times['2'] = 30;
    times['3'] = 60;
    times['4'] = 90;
    times['5'] = 120;
    times['6'] = 150;
    times['7'] = 180;
    times['8'] = 210;
    times['9'] = 240;
    times['A'] = 270;
    times['B'] = 300;
    times['C'] = 330;
    times['D'] = 360;
    times['E'] = 390;
    times['F'] = 420;
    times['G'] = 450;
    times['H'] = 480;
}

void cycle(int zone, int &time, double &money, int day){
    if(zone >= 100 && zone <=199){
        if(time >= 300)
            money += 7.5;
        else
            money += 5;
    }else if(zone >= 200 && zone <= 299){
        if(time >= 360)
            money += 7.5;
        else
            money += 3.75;
    }else if(zone >= 300 && zone <= 399){
        if(time >= 240)
            money += 5.25;
        else
            money += 4.625;
    }else if(zone >= 400 && zone <= 499){
        if(day == 1 || day == 7)
            money += 6.75;
        else
            money += 3.375;
    }else if(zone >= 500 && zone <= 599){
        if(time >= 360)
            money += 6;
        else
            money += 4;
    }
    time += 30;
}

int main() {
    freopen("timesheets.in", "r", stdin);
    buildMap();
    for (int acsl = 0; acsl < 5; ++acsl) {
        double money = 0;
        for (int i = 0; i < 2; ++i) {
            if(i)
                scanf(", ");
            int time = 0, zone, day;
            char time1, time2;
            int start, finish;
            scanf("%d, %d, %c, %c",&zone, &day, &time1, &time2);
            start = times[time1];
            finish = times[time2];
            for (; start < finish; cycle(zone, time, money, day), start+=30);
        }
        scanf("\n");
        printf("$%.2lf\n", money + .0049);
    }
    return 0;
}
