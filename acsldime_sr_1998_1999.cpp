#include <iostream>
#include <cstring>
#include <cstdio>
using namespace std;

int factorial(int n){
    if(n == 0)
        return 1;
    return factorial(n - 1) * n;
}

double pow(double n, int power){
    if(power == 0)
        return 1;
    return n * pow(n, power - 1);
}

double probability(int n, int k, double p){
    return (double)factorial(n) / (double)(factorial(n - k) * factorial(k)) * pow(p, k) * pow(1 - p, n - k);
}

double between(int n, int s, int t, double p){
    double result = 0;
    for (int i = s; i <= t; ++i) {
        result += probability(n, i, p);
    }
    return result;
}

double atleast(int n, int k, double p){
    return between(n, k, n, p);
}

double atmost(int n, int k, double p){
    return between(n, 0, k, p);
}

int main() {
    freopen("dime.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        char str[10];
        int n, k, t = 0;
        double p;
        scanf("%s %d, %lf, %d", str, &n, &p, &k);
        if(strcmp(str, "BETWEEN,") == 0){
            scanf(", %d", &t);
        }
        scanf("\n");
        if(strcmp(str, "EQUAL,") == 0){
            printf("%.3lf\n", probability(n, k, p));
        }else if(strcmp(str, "ATLEAST,") == 0){
            printf("%.3lf\n", atleast(n, k, p));
        }else if(strcmp(str, "ATMOST,") == 0){
            printf("%.3lf\n", atmost(n, k, p));
        }else if(strcmp(str, "BETWEEN,") == 0){
            printf("%.3lf\n", between(n, k, t, p));
        }
    }
    return 0;
}
