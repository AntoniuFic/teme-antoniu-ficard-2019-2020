#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace std;

map<int, pair<int, int>>coordinates;
ifstream f("fifteenpuzzle.in");

void buildMap(){
    for(int i=0, tile = 1; i<4; ++i){
        for(int j=0; j<4; ++j, ++tile)
            coordinates[tile] = make_pair(i, j);
    }
}

int main()
{
    buildMap();
    for(int acsl = 0; acsl < 10; ++acsl){
        int location = 0;
        pair<int, int> c = coordinates[location];
        string characters;
        f>>location;
        getline(f, characters);
        for(int i = 0; i<characters.length(); ++i){
            switch(characters[i]){
                case 'R':
                    c.second++;
                    location++;
                    break;
                case 'L':
                    c.second--;
                    location--;
                    break;
                case 'A':
                    c.first--;
                    location-=4;
                    break;
                case 'B':
                    c.first++;
                    location+=4;
                    break;
            }
        }
        cout<<location<<"\n";
    }
    return 0;
}
