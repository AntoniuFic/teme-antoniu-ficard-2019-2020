#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> split(int x){
    vector<int> digits;
    for(; x; x/=10){
        digits.push_back(x % 10);
    }
    reverse(digits.begin(), digits.end());
    return digits;
}

int merge(vector<int> digits){
    int x = 0;
    for (int i = 0; i < digits.size(); ++i) {
        x *= 10;
        x += digits[i];
    }
    return x;
}

int digit(vector<int> digits){
    int sum = 0;
    for (int i = 0; i < digits.size(); ++i) {
        sum += digits[i];
    }
    sum /= digits.size();
    int maxd = -1, maxpos = -1;
    for (int i = 0; i < digits.size(); ++i) {
            if(digits[i] > maxd && digits[i] <= sum)
                maxd = digits[i], maxpos = i;
    }
    return maxpos;
}

int main() {
    ifstream f("digits.in");
    for (int acsl = 0; acsl < 5; ++acsl) {
        int number;
        f>>number;
        vector<int> digits = split(number);
        int pos = digit(digits);
        int maxi = -1, mini = 10;
        int sum = 0;
        for (int i = 0; i < digits.size(); ++i) {
            maxi = max(maxi, digits[i]);
            mini = min(mini, digits[i]);
            sum += digits[i];
        }
        sum = sum % 10;
        if(pos >= 0){
            if(digits[pos] < 3)
                digits[pos] = maxi;
            else if(digits[pos] < 6)
                digits[pos] = mini;
            else if(digits[pos] < 9)
                digits[pos] = sum;
            else
                digits[pos] = 0;
        }
        number = merge(digits);
        cout<<number<<"\n";
    }
    return 0;
}
