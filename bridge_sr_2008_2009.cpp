#include <iostream>
#include <cstdio>
#include <map>

using namespace std;

int main() {
    freopen("bridge.in", "r", stdin);
    map<char, int>scores;
    scores['T'] = 30;
    scores['H'] = 30;
    scores['S'] = 30;
    scores['C'] = 20;
    scores['D'] = 20;
    int vulTeam = -1;
    int scoreA[2] = {0, 0};
    int scoreU[2] = {0, 0};
    for (int acsl = 0; acsl < 5; ++acsl) {
        if(scoreU[0] >= 100 || scoreU[1] >= 100)
            scoreU[0] = 0, scoreU[1] = 0;
        int team, won, bid, other;
        char suite;
        scanf("%d, %d, %d, %c\n", &team, &bid, &won, &suite);
        if(team == 1)
            team = 0, other = 1;
        else if(team == 2)
            team = 1, other = 0;
        bid += 6;
        if(bid > won)
            if(vulTeam == team)
                scoreA[other] += 100 * (bid - won);
            else
                scoreA[other] += 50 * (bid - won);
        else{
            scoreU[team] += (bid - 6) * scores[suite];
            if(suite == 'T')
                scoreU[team] += 10;
            scoreA[team] += (won - bid) * scores[suite];
        }
        if(won >= bid){
            vulTeam = team;
        }else vulTeam = other;
        cout<<scoreU[0]<<", "<<scoreA[0]<<", "<<scoreU[1]<<", "<<scoreA[1]<<"\n";
    }
    return 0;
}
