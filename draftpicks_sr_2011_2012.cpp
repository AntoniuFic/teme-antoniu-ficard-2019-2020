#include <iostream>
#include <cstdio>
#include <algorithm>


using namespace std;

struct draft{
    int years, value, guaranteed, annualSalary;
    bool operator<(draft other){
        return this->annualSalary < other.annualSalary;
    }
}player[10];

int expectedValue(draft play, int gamesInSeason){
    double chanceOfInjury;
    if(gamesInSeason == 16)
        chanceOfInjury = 3;
    else if(gamesInSeason == 18)
        chanceOfInjury = 3.375;
    return (1 - ((double)play.years * chanceOfInjury) / 100)*(double)play.value + (double)play.years * chanceOfInjury / 100 * (double)play.guaranteed;
}

int main() {
    freopen("draftpicks.in", "r", stdin);
    int min = 0, max = 0;
    for (int acsl = 0; acsl < 10; ++acsl) {
        double value, guaranteed;
        scanf("%d, %lf, %lf\n", &player[acsl].years, &value, &guaranteed);
        player[acsl].value = value * 1000000;
        player[acsl].guaranteed = guaranteed * 1000000;
        player[acsl].annualSalary = player[acsl].value / player[acsl].years;
        if(player[acsl].annualSalary > player[max].annualSalary)
            max = acsl;
        if(player[acsl].annualSalary < player[min].annualSalary)
            min = acsl;
    }
    cout<<(player[max].annualSalary - player[min].annualSalary) / 16<<"\n"<<(player[max].annualSalary + player[min].annualSalary) / 36<<"\n";
    int maxE = 0;
    for (int i = 0; i < 10; ++i) {
       int expected = expectedValue(player[i], 16);
       if(expected > expectedValue(player[maxE], 16))
           maxE = i;
    }
    cout<<expectedValue(player[maxE], 16)<<" by #"<<maxE + 1<<"\n";
    int average = 0;
    for (int i = 0; i < 10; ++i) {
        average += expectedValue(player[i], 18);
    }
    sort(player, player + 10);
    cout<<average / 10<<"\n"<<(player[4].annualSalary + player[5].annualSalary) / 2;
    return 0;
}
