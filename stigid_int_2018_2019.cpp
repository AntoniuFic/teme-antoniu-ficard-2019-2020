#include <iostream>
#include <string>
#include <fstream>

using namespace std;

ifstream f("stigid.in");


unsigned long long tenToN(int p){
    unsigned long long rez=1;
    unsigned long long fact=10;
    for(int bit=0; p>>bit; bit++){
        if((p>>bit)&1)
            rez=(rez*fact);
        fact=(fact*fact);
    }
    return rez;
}

unsigned long long buildNumber(string number, int n){
    unsigned long long result = 0;
    for (int i = 0; i < n; ++i) {
        result *= 10;
        result += number[i] - '0';
    }
    return result;
}

unsigned long long nextNumber(string number, unsigned long long power, unsigned long long &prevNumber, unsigned long long pos, int n){
    prevNumber -= power * (number[pos - 1] - '0');
    prevNumber *= 10;
    prevNumber += (number[pos + n - 1] - '0');
    return prevNumber;
}

int main() {
    for (int acsl = 0; acsl < 5; ++acsl) {
        string number;
        int n;
        f>>number>>n;
        unsigned long long Number = buildNumber(number, n);
        unsigned long long sum = Number;
        unsigned long long power = tenToN(n - 1);
        for (int i = 1; i < number.length() - n + 1; ++i) {
            sum += nextNumber(number, power, Number, i, n);
        }
        cout<<sum<<"\n";
    }
    return 0;
}
