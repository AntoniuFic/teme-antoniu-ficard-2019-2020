#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <cstdio>

using namespace std;

ifstream f("chmod.in");

int main() {
    map<string, int> bits;
    bits["000"] = 0;
    bits["001"] = 1;
    bits["010"] = 2;
    bits["011"] = 3;
    bits["100"] = 4;
    bits["101"] = 5;
    bits["110"] = 6;
    bits["111"] = 7;
    for (int acsl = 0; acsl < 2; ++acsl) {
        int n1, n2, n3;
        char comma;
        f>>n1>>comma>>n2>>comma>>n3;
        string owner = "---", group = "---", others = "---";
        if((n1 >> 2) & 1)
            owner[0] = 'r';
        if((n1 >> 1) & 1)
            owner[1] = 'w';
        if(n1 & 1)
            owner[2] = 'x';
        if((n2 >> 2) & 1)
            group[0] = 'r';
        if((n2 >> 1) & 1)
            group[1] = 'w';
        if(n2 & 1)
            group[2] = 'x';
        if((n3 >> 2) & 1)
            others[0] = 'r';
        if((n3 >> 1) & 1)
            others[1] = 'w';
        if(n3 & 1)
            others[2] = 'x';
        cout<<((n1 >> 2) & 1)<<((n1 >> 1) & 1)<<(n1 & 1)<<' '<<((n2 >> 2) & 1)<<((n2 >> 1) & 1)<<(n2 & 1)<<' '<<((n3 >> 2) & 1)<<((n3 >> 1) & 1)<<(n3 & 1)<<" and "<<owner<<" "<<group<<" "<<others<<"\n";
    }
    f.get();
    for (int acsl = 0; acsl < 2; ++acsl) {
        int n1 = 0, n2 = 0, n3 = 0;
        string owner = "---", group = "---", others = "---";
        string bits1, bits2, bits3;
        getline(f, bits1, ',');
        f.get();
        getline(f, bits2, ',');
        f.get();
        getline(f, bits3);
        for (int i = 0; i < 3; ++i) {
            if(bits1[i] == '1')
                n1 += (1 << (2 - i));
            if(bits2[i] == '1')
                n2 += (1 << (2 - i));
            if(bits3[i] == '1')
                n3 += (1 << (2 - i));
        }
        if((n1 >> 2) & 1)
            owner[0] = 'r';
        if((n1 >> 1) & 1)
            owner[1] = 'w';
        if(n1 & 1)
            owner[2] = 'x';
        if((n2 >> 2) & 1)
            group[0] = 'r';
        if((n2 >> 1) & 1)
            group[1] = 'w';
        if(n2 & 1)
            group[2] = 'x';
        if((n3 >> 2) & 1)
            others[0] = 'r';
        if((n3 >> 1) & 1)
            others[1] = 'w';
        if(n3 & 1)
            others[2] = 'x';
        cout<<n1<<n2<<n3<<" and "<<owner<<" "<<group<<" "<<others<<"\n";
    }
    int n1 = 0, n2 = 0, n3 = 0;
    string owner, group, others;
    getline(f, owner, ',');
    f.get();
    getline(f, group, ',');
    f.get();
    getline(f, others);
    for (int i = 0; i < 3; ++i) {
        if(owner[i] != '-')
            n1 += (1 << (2 - i));
        if(group[i] != '-')
            n2 += (1 << (2 - i));
        if(others[i] != '-')
            n3 += (1 << (2 - i));
    }
    cout<<n1<<n2<<n3<<" and "<<((n1 >> 2) & 1)<<((n1 >> 1) & 1)<<(n1 & 1)<<' '<<((n2 >> 2) & 1)<<((n2 >> 1) & 1)<<(n2 & 1)<<' '<<((n3 >> 2) & 1)<<((n3 >> 1) & 1)<<(n3 & 1);
    return 0;
}
