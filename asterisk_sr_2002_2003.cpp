#include <iostream>

using namespace std;

bool matrix[100][100];

void reset(){
    for (int i = 0; i < 100; ++i) {
        for (int j = 0; j < 100; ++j) {
            matrix[i][j] = 0;
        }
    }
}

void putS(int h, int w, int x, int y){
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < h; ++j){
            matrix[x + i][y + j] = 1;
        }
    }
}

void putH(int h, int w, int x, int y){
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < h - i; ++j) {
            matrix[x + i][y + h - j] = 1;
        }
    }
}

void putL(int h, int w, int x, int y){
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j <= i; ++j) {
            matrix[x + i][y + j] = 1;
        }
    }
}

void putB(int h, int w, int x, int y){
    for (int i = 0; i < h; ++i) {
        for (int j = 0; j < w; ++j) {
            matrix[x + i][y + j] = 1;
        }
    }
}

void putT(int h, int w, int x, int y){
    h+=2;
    h/=3;
    for (int i = h - 1; i < 2 * h - 1; ++i){
        for (int j = 0; j < 3 * h - 2; ++j) {
            matrix[x + j][y + i] = 1;
        }
    }
    for (int i = h - 1; i < 2 * h - 1; i += h - 1){
        for (int j = 0; j < 3 * h - 2; ++j) {
            matrix[x + i][y + j] = 1;
        }
    }
}

void repeater(int h, int w, int across, int rows, void put(int, int, int, int)){
    for (int i = 0; i < (h - 1) * (rows - 1) + 1; i += (h - 1)) {
        for (int j = 0; j < (w - 1) * (across - 1) + 1; j += (w - 1)) {
            put(h, w, i, j);
        }
    }
    for (int i = 0; i < (h - 1) * rows + 1; ++i) {
        for (int j = 0; j < (w - 1) * across + 1; ++j) {
            if(matrix[i][j])
                cout<<"* ";
            else
                cout<<"  ";
        }
        cout<<"\n";
    }
}

void selector(char type, int h, int w, int a, int r){
    switch(type){
        case 'S':
            repeater(h, h, a, r, putS);
            break;
        case 'H':
            repeater(h, h, a, r, putH);
            break;
        case 'L':
            repeater(h, h, a, r, putL);
            break;
        case 'B':
            repeater(h, w, a, r, putB);
            break;
        case 'T':
            repeater(3 * h - 2, 3 * h - 2, a, r, putT);
    }
}

int main() {
    freopen("asterisk.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        reset();
        char type;
        int h, w, a, r;
        scanf("%c, ", &type);
        if(type == 'B')
            scanf("%d, %d, %d, %d\n", &h, &w, &a, &r);
        else
            scanf("%d, %d, %d\n", &h, &a, &r);
        selector(type, h, w, a, r);
        cout<<endl<<endl;
    }
    return 0;
}
