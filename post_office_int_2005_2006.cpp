#include <iostream>
#include <cstdio>

using namespace std;

double calculate(double price, double increments, double weight){
    double result = 0;
    for (double current = 0; current < weight;) {
        result += price;
        current += increments;
    }
    return result;
}

double price(double length, double width, double height, double weight){
    if(length >= 3.5 && length <= 4.25 && width >= 3.5 && width <=6 && height >= 0.007 && height <= 0.016)
        return calculate(.2, .0625, weight);
    if(length > 4.25 && length < 6 && width > 6 && width < 11.5 && height >= 0.007 && height <= 0.016)
        return calculate(.3, .0625, weight);
    if(length >= 3.5 && length <= 6.125 && width >= 5 && width <= 11.5 && height > 0.016 && height < 0.25)
        return calculate(.47, .0625, weight);
    if(length > 6.125 && length < 24 && width >= 11 && width <= 18 && height >= 0.25 && height <= 0.5)
        return calculate(.56, .0625, weight);
    if(length + 2 * height + 2 * width <= 84 && length >= 24 && width > 18 && height > 0.5)
        return calculate(1.5, .5, weight);
    if(length + 2 * height + 2 * width <= 130 && length + 2 * height + 2 * width > 84)
        return calculate(1.75, .5, weight);
    return -1;
}

int main() {
    freopen("postoffice.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        double length, width, height, weight;
        scanf("%lf, %lf, %lf, %lf\n", &length, &width, &height, &weight);
        double result = price(length, width, height, weight);
        if(result < 0){
            cout<<"UNMAILABLE\n";
            continue;
        }
        printf("%.2lf\n", result);
    }
    return 0;
}
