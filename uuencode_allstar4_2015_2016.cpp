#include <iostream>
#include <fstream>

using namespace std;

ifstream f("uuencode.in");

int main() {
    for (int acsl = 0; acsl < 10; ++acsl) {
        string s;
        getline(f, s);
        for(; s.length() % 3;)
            s.push_back('0');
        bool bits[500];
        int k = 0;
        for (int i = 0; i < s.length(); ++i) {
            for (int j = 7; j >= 0; --j) {
                bits[k++] = ((((int)s[i]) >> j) & 1);
            }
        }
        s.clear();
        for (int i = 0, aux = 0, shifter = 5; i < k; ++i, --shifter) {
            aux += (((int)bits[i]) << shifter);
            if(shifter == 0){
                if(aux == 0)
                    aux = 126 - 32;
                s.push_back((char)(aux + 32));
                shifter = 6;
                aux = 0;
            }
        }
        cout<<s<<"\n";
    }
    return 0;
}
