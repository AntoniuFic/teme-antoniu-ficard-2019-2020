#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

int step(int current, int step){
//    if(step > 0)
//        step -= 1;
//    else if(step < 0)
//        step += 1;
    current += step;
    while(current > 26)
        current -= 26;
    while(current < 1)
        current += 26;
    return current;
}

int sumf(int input){
    int sum = 0;
    for (int i = 1; i <= input; ++i) {
        if(input % i == 0)
            sum += i;
    }
    return sum;
}

int rule(int input, int r){
    switch(r){
        case 1: return 2 * input;
        case 2: return (input % 3) * 5;
        case 3: return (input / 4) * -8;
        case 4: return (int)sqrt(input) * -12;
        case 5: return sumf(input) * 10;
    }
}

int main() {
    freopen("code.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        char letter;
        int ru, current = 1;
        do{
            scanf("%c", &letter);
            if(letter == '$')
                break;
            scanf(",%d,", &ru);
            current = step(current, rule(letter - 'A' + 1, ru));
            cout<<(char)('A' + current - 1);
        }while(1);
        scanf("\n");
        cout<<endl;
    }
    return 0;
}
