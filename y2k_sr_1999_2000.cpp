#include <iostream>

using namespace std;

int main() {
    freopen("y2k.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        int n1, n2, q;
        int fr[10] = {0};
        scanf("%d, %d, %d\n", &n1, &n2, &q);
        q = n2 - n1;
        for (int i = 0; i < 2000; ++i, n1+=q) {
            n2 = abs(n1);
            while(n2){
                fr[n2 % 10]++;
                n2/=10;
            }
        }
        int maxid = 0, dig[10], k = 0;
        for (int j = 0; j < 10; ++j) {
            if(fr[j] > maxid){
                maxid = fr[j], k = 1, dig[0] = j;
            }else if(fr[j] == maxid)
                dig[k++] = j;
        }
        cout<<dig[0];
        for (int i = 1; i < k; ++i) {
            cout<<" and "<<dig[i];
        }
        cout<<endl;
    }
    return 0;
}
