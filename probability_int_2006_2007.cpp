#include <iostream>
#include <cstdio>

using namespace std;

struct fraction{
    int top, bottom;
    fraction(int top, int bottom){
        this->top = top;
        this->bottom = bottom;
    }
    fraction operator* (fraction other){
        fraction result(0, 0);
        result.top = this->top * other.top;
        result.bottom = this->bottom * other.bottom;
        return result;
    }
    fraction negation(){
        fraction result(0, 0);
        result.top = this->bottom - this->top;
        result.bottom = this->bottom;
        return result;
    }
    fraction reduced(bool Numerator = false){
        fraction result(0, 0);
        result.top = this->top;
        if(Numerator)
            result.top--;
        result.bottom = this->bottom - 1;
        return result;
    }
    friend ostream& operator<<(ostream& os, const fraction fr){
        os<< fr.top << "/" << fr.bottom;
        return os;
    }
};

fraction calculate(int balls, int reds, int blues, char *types, bool ordered){
    fraction result(1, 1), pickReds(reds, reds + blues), pickBlues(blues, reds + blues);
    for(int i = 0; i < balls; ++i){
        switch(types[i]) {
            case 'R':
                result = result * pickReds;
                if (ordered)
                    pickReds = pickReds.reduced(true), pickBlues = pickBlues.reduced();
                break;
            case 'B':
                result = result * pickBlues;
                if (ordered)
                    pickReds = pickReds.reduced(), pickBlues = pickBlues.reduced(true);
                break;
            case 'r':
                result = result * pickReds.negation();
                if (ordered)
                    pickReds = pickReds.reduced(), pickBlues = pickBlues.reduced();
                break;
            case 'b':
                result = result * pickBlues.negation();
                if (ordered)
                    pickReds = pickReds.reduced(), pickBlues = pickBlues.reduced();
                break;
        }
    }
    return result;
}

int main()
{
    freopen("probability.in", "r", stdin);
    for(int acsl = 0; acsl < 5; ++acsl){
        int balls, reds, blues;
        bool ordered;
        char yesnot = 'Y';
        scanf("%d, %d, %d", &balls, &reds, &blues);
        if(balls >= 2)
            scanf(", %c", &yesnot);
        char *types = new char[balls];
        ordered = yesnot != 'Y';
        for(int i = 0; i < balls; ++i)
            scanf(", %c", &types[i]);
        scanf("\n");
        cout<<calculate(balls, reds, blues, types, ordered)<<"\n";
        delete[] types;
    }
    return 0;
}
