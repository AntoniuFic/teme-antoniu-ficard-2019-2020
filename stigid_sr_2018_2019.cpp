#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ifstream f("stigid.in");

long long buildNumber(string number){
    long long result = 0;
    for(int i=0; i<number.length(); ++i, result *= 10)
        result += number[i] - '0';
    return result / 10;
}

int main()
{
    for(int acsl = 0; acsl < 5; ++acsl){
        string number;
        long long n;
        long long sum = 0;
        f>>number>>n;
        for(; number.length() % n != 0; number += '0');
        for(int i=0; i<number.length(); i+=n){
            sum += buildNumber(number.substr(i, n));
        }
        cout<<sum<<"\n";
    }
    return 0;
}
