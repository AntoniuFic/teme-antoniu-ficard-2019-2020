#include <iostream>
#include <cstdio>

using namespace std;

FILE *f = freopen("chmod.in", "r", stdin);

int main() {
    for (int acsl = 0; acsl < 5; ++acsl) {
        int n1, n2, n3, control;
        scanf("%d, %d, %d, %d\n", &control, &n1, &n2, &n3);
        string owner = "---", group = "---", others = "---";
        if((n1 >> 2) & 1)
            owner[0] = 'r';
        if((n1 >> 1) & 1)
            owner[1] = 'w';
        if(n1 & 1)
            owner[2] = 'x';
        if(control == 1 && n1 & 1)
            owner[2] = 's';
        if((n2 >> 2) & 1)
            group[0] = 'r';
        if((n2 >> 1) & 1)
            group[1] = 'w';
        if(n2 & 1)
            group[2] = 'x';
        if(control == 2 && n2 & 1)
            group[2] = 's';
        if((n3 >> 2) & 1)
            others[0] = 'r';
        if((n3 >> 1) & 1)
            others[1] = 'w';
        if(n3 & 1)
            others[2] = 'x';
        if(control == 4 && n3 & 1)
            others[2] = 't';
        cout<<((n1 >> 2) & 1)<<((n1 >> 1) & 1)<<(n1 & 1)<<' '<<((n2 >> 2) & 1)<<((n2 >> 1) & 1)<<(n2 & 1)<<' '<<((n3 >> 2) & 1)<<((n3 >> 1) & 1)<<(n3 & 1)<<" and "<<owner<<" "<<group<<" "<<others<<"\n";
    }
    return 0;
}
