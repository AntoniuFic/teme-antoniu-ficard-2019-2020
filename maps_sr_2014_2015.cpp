#include <iostream>
#include <fstream>
#include <map>

using namespace std;

map<char, int>distances;

ifstream f("maps.in");

struct driver{
    int speed, deptTime;
    char location;
};

void buildMap(){
    distances['A'] = 450;
    distances['B'] = 140;
    distances['C'] = 125;
    distances['D'] = 365;
    distances['E'] = 250;
    distances['F'] = 160;
    distances['G'] = 380;
    distances['H'] = 235;
    distances['J'] = 320;
}

int main() {
    buildMap();
    for (int acsl = 0; acsl < 5; ++acsl) {
        driver driver1{}, driver2{};
        char comma;
        f>>driver1.location>>comma>>driver2.location>>comma>>driver1.deptTime>>comma;
        char tz;
        f>>tz;
        if(tz == 'P' && driver1.deptTime < 12)
            driver1.deptTime += 12;
        else if(tz == 'A' && driver1.deptTime == 12)
            driver1.deptTime -= 12;
        f>>tz>>comma>>driver2.deptTime>>comma>>tz;
        if(tz == 'P' && driver2.deptTime < 12)
            driver2.deptTime += 12;
        else if(tz == 'A' && driver2.deptTime == 12)
            driver2.deptTime -= 12;
        f>>tz>>comma>>driver1.speed>>comma>>driver2.speed;
        int distance = 0;
        char departureLocation = driver1.location;
        while(departureLocation != driver2.location){
            distance+=distances[departureLocation];
            ++departureLocation;
        }
        if(driver1.deptTime > driver2.deptTime && driver1.deptTime - driver2.deptTime > 12 && driver2.deptTime + 24 - driver1.deptTime < driver1.deptTime - driver2.deptTime)
            driver1.deptTime -= 12, driver2.deptTime += 12;
        else if(driver1.deptTime < driver2.deptTime && driver2.deptTime - driver1.deptTime > 12 && driver1.deptTime + 24 - driver2.deptTime < driver2.deptTime - driver1.deptTime)
            driver2.deptTime -= 12, driver1.deptTime += 12;
        if(driver1.deptTime < driver2.deptTime)
            distance -= (driver2.deptTime - driver1.deptTime) * driver1.speed;
        else if(driver1.deptTime > driver2.deptTime)
            distance -= (driver1.deptTime - driver2.deptTime) * driver2.speed;
        double driveTime = (double)distance / (double)(driver1.speed + driver2.speed);
        if(driver1.deptTime < driver2.deptTime)
            driveTime += driver2.deptTime - driver1.deptTime;
        int hours = driveTime;
        driveTime*=60;
        driveTime -= hours * 60;
        cout<<hours<<":";
        if((int)(driveTime + .5) < 10)
            cout<<0;
        cout<<(int)(driveTime + .5)<<"\n";
    }
    return 0;
}
