#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    freopen("poker.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        vector<int> cards[13];
        int given[5];
        int suits[4] = {0, 0, 0, 0};
        scanf("%d, %d, %d, %d, %d\n", &given[0], &given[1], &given[2], & given[3], &given[4]);
        for (int i = 0; i < 5; ++i) {
            if(given[i] > 0 && given[i] < 14){
                cards[given[i] - 1].push_back(1);
                given[i]--;
                suits[0]++;
            }
            else if(given[i] > 13 && given[i] < 27){
                cards[given[i] - 14].push_back(2);
                given[i] -= 14;
                suits[1]++;
            }
            else if(given[i] > 26 && given[i] < 40){
                cards[given[i] - 27].push_back(3);
                given[i] -= 27;
                suits[2]++;
            }
            else if(given[i] > 39 && given[i] < 53){
                cards[given[i] - 40].push_back(4);
                given[i] -= 40;
                suits[3]++;
            }
        }
        bool done = false;
        for (int i = 0; i < 13 && !done; ++i) {
            if(cards[i].size() == 4){
                cout<<"FOUR OF A KIND\n";
                done = true;
            }
        }
        if(done)
            continue;
        int three = 0, pair = 0;
        for (int i = 0; i < 13; ++i) {
            if(cards[i].size() == 2)
                ++pair;
            if(cards[i].size() == 3)
                ++three;
        }
        if(three && pair){
            cout<<"FULL HOUSE\n";
            continue;
        }
        int suitsno = 0;
        for (int i = 0; i < 4 && !done; ++i) {
            if(suits[i] != 0){
                suitsno++;
            }
            if(suits[i] == 5){
                cout<<"FLUSH\n";
                done = true;
            }
        }
        if(done)
            continue;
        sort(given, given + 5);
        bool consecutive = true;
        for (int i = 0; i < 4 && consecutive; ++i) {
            if(given[i] != given[i + 1] - 1)
                consecutive = false;
        }
        if(consecutive && suitsno > 1){
            cout<<"STRAIGHT\n";
            continue;
        }
        if(three){
            cout<<"THREE OF A KIND\n";
            continue;
        }
        if(pair > 1){
            cout<<"TWO PAIRS\n";
            continue;
        }
        if(pair){
            cout<<"PAIR\n";
            continue;
        }
        cout<<"NONE\n";
    }
    return 0;
}
