#include <iostream>
#include <cstdio>
#include <vector>

#define tests 5

using namespace std;

pair<int, int> split(int x){
    return make_pair((x - 1) / 5 + 1, (x - 1) % 5 + 1);
}

int matrix[7][7];


int merge(pair<int, int> x){
    return (x.first - 1) * 5 + x.second;
}

void reset(){
    for (int i = 0; i < 7; ++i) {
        for (int j = 0; j < 7; ++j) {
            matrix[i][j] = 0;
        }
    }
    for (int i = 0; i < 7; ++i) {
        matrix[i][0] = matrix[0][i] = matrix[6][i] = matrix[i][6] = -1;
    }
}

int canMoveIn(pair<int, int> _split, int side){
    const int di[] = {-1, 0, 1, 0};
    const int dj[] = {0, 1, 0, -1};
    if(matrix[_split.first][_split.second] > 0)
        return -1;
    for (int v = 0; v < 4; ++v) {
        if(matrix[_split.first + di[v]][_split.second + dj[v]] != side && matrix[_split.first + di[v]][_split.second + dj[v]] > 0)
            return merge(make_pair(_split.first + di[v], _split.second + dj[v]));
    }
    return -1;
}

int inDanger(int coord){
    const int di[] = {0, 1, -1, 0};
    const int dj[] = {-1, 0, 0, 1};
    if(coord == 13)
        return -1;
    pair<int, int> _split = split(coord);
    for (int i = 0; i < 4; ++i) {
        if(matrix[_split.first + di[3 - i]][_split.second + dj[3 - i]] == -1)
            continue;
        int theMover = canMoveIn(make_pair(_split.first + di[3 - i], _split.second + dj[3 - i]), matrix[_split.first][_split.second]);
        if(matrix[_split.first][_split.second] != matrix[_split.first + di[i]][_split.second + dj[i]] && matrix[_split.first + di[i]][_split.second + dj[i]] > 0 && theMover != -1)
            return theMover;
    }
    return -1;
}

int defend(int coord){
    const int di[] = {0, 0, -1, 1};
    const int dj[] = {1, -1, 0, 0};
    if(matrix[3][3] == 0 && (coord == 12 || coord == 14 || coord == 18 || coord == 8))
        return 13;
    pair<int, int> _split = split(coord);
    if(matrix[_split.first - 1][_split.second] == 0 && matrix[_split.first - 1][_split.second - 1] != matrix[_split.first][_split.second] && matrix[_split.first - 1][_split.second - 1] > 0 && matrix[_split.first - 1][_split.second + 1] != matrix[_split.first][_split.second] && matrix[_split.first - 1][_split.second + 1] > 0)
        return merge(make_pair(_split.first - 1, _split.second));
    if(matrix[_split.first][_split.second + 1] == 0 && matrix[_split.first - 1][_split.second + 1] != matrix[_split.first][_split.second] && matrix[_split.first - 1][_split.second + 1] > 0 && matrix[_split.first + 1][_split.second + 1] != matrix[_split.first][_split.second] && matrix[_split.first + 1][_split.second + 1] > 0)
        return merge(make_pair(_split.first, _split.second + 1));
    if(matrix[_split.first + 1][_split.second] == 0 && matrix[_split.first + 1][_split.second + 1] != matrix[_split.first][_split.second] && matrix[_split.first + 1][_split.second + 1] > 0 && matrix[_split.first + 1][_split.second - 1] != matrix[_split.first][_split.second] && matrix[_split.first + 1][_split.second - 1] > 0)
        return merge(make_pair(_split.first + 1, _split.second));
    if(matrix[_split.first][_split.second - 1] == 0 && matrix[_split.first + 1][_split.second - 1] != matrix[_split.first][_split.second] && matrix[_split.first + 1][_split.second - 1] > 0 && matrix[_split.first - 1][_split.second - 1] != matrix[_split.first][_split.second] && matrix[_split.first - 1][_split.second - 1] > 0)
        return merge(make_pair(_split.first, _split.second - 1));
    for (int v = 0; v < 4; ++v) {
        if(matrix[_split.first + di[v]][_split.second + dj[v]] == 0){
            matrix[_split.first + di[v]][_split.second + dj[v]] = matrix[_split.first][_split.second];
            int result = inDanger(merge(make_pair(_split.first + di[v], _split.second + dj[v])));
            matrix[_split.first + di[v]][_split.second + dj[v]] = 0;
            if(result == -1)
                return merge(make_pair(_split.first + di[v], _split.second + dj[v]));
        }
    }
    return -1;
}

int attack(int coord, int attacked){
    const int di[] = {0, 1, -1, 0};
    const int dj[] = {-1, 0, 0, 1};
    pair<int, int> _split = split(attacked);
    int dir = -1;
    for (int i = 0; i < 4; ++i) {
        int theMover = canMoveIn(make_pair(_split.first + di[3 - i], _split.second + dj[3 - i]), matrix[_split.first][_split.second]);
        if(matrix[_split.first][_split.second] != matrix[_split.first + di[i]][_split.second + dj[i]] && matrix[_split.first + di[i]][_split.second + dj[i]] > 0 && theMover == coord)
            dir = i;
    }
    return merge(make_pair(_split.first + di[3 - dir], _split.second + dj[3 - dir]));
}

int main() {
    freopen("seega.in", "r", stdin);
    for (int acsl = 0; acsl < tests; ++acsl) {
        reset();
        int numberOfPieces;
        vector<int>xs, os, xd, od;
        scanf("%d", &numberOfPieces);
        for (int i = 0; i < numberOfPieces; ++i) {
            int x;
            scanf(", %d", &x);
            pair<int, int> coord = split(x);
            matrix[coord.first][coord.second] = 1;
            xs.push_back(x);
        }
        for (int i = 0; i < numberOfPieces; ++i) {
            int o;
            scanf(", %d", &o);
            pair<int, int> coord = split(o);
            matrix[coord.first][coord.second] = 2;
            os.push_back(o);
        }
        scanf("\n");
        int minx = 40, mino = 40;
        int minxa = 1, minxo = 1;
        for (int i = 0; i < numberOfPieces; ++i) {
            xd.push_back(inDanger(xs[i]));
            od.push_back(inDanger(os[i]));
            if(xd[i] < minx && xd[i] > 0)
                minx = xd[i], minxa = xs[i];
            if(od[i] < mino && od[i] > 0)
                mino = od[i], minxo = os[i];
        }
        int output = -1;
        if(mino < 40){
            output = attack(mino, minxo);
        }else{
            bool ok = false;
            do{
                ok = false;
                for (int i = 0; i < numberOfPieces - 1; ++i) {
                    if(xs[i] > xs[i + 1]){
                        swap(xs[i], xs[i + 1]);
                        swap(xd[i], xd[i + 1]);
                        ok = true;
                    }
                }
            }while(ok);
            for (int i = 0; i < numberOfPieces && !ok; ++i) {
                if(xd[i] > 0 && defend(xs[i]) > 0){
                    output = defend(xs[i]);
                    ok = true;
                }
            }
            for (int i = 0; i < numberOfPieces && !ok; ++i) {
                if(defend(xs[i]) > 0){
                    output = defend(xs[i]);
                    ok = true;
                }
            }
        }
        if(output < 0)
            cout<<"LOSE A TURN\n";
        else
            cout<<output<<"\n";
        if(minx < 40){
            cout<<attack(minx, minxa)<<"\n";
        }else{
            bool ok = false;
            do{
                ok = false;
                for (int i = 0; i < numberOfPieces - 1; ++i) {
                    if(os[i] > os[i + 1]){
                        swap(os[i], os[i + 1]);
                        swap(od[i], od[i + 1]);
                        ok = true;
                    }
                }
            }while(ok);
            for (int i = 0; i < numberOfPieces && !ok; ++i) {
                if(od[i] > 0 && defend(os[i]) > 0){
                    cout<<defend(os[i])<<"\n";
                    ok = true;
                }
            }
            for (int i = 0; i < numberOfPieces && !ok; ++i) {
                if(defend(os[i]) > 0){
                    cout<<defend(os[i])<<"\n";
                    ok = true;
                }
            }
        }
    }
    return 0;
}
