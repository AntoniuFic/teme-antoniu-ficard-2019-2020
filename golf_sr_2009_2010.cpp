#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;

bool comparer(pair<char, int>first, pair<char, int>second){
    return first.second < second.second;
}

int main() {
    int allscores[40], k=0;
    freopen("golf.in", "r", stdin);
    const int pars[] = {3, 4, 5, 4, 4, 4, 5, 3, 4}, par = 3 + 4 + 5 + 4 + 4 + 4 + 5 + 3 + 4;
    int scores[9][4], score[] = {0, 0, 0, 0}, wins[] = {0, 0, 0, 0};
    for (int i = 0; i < 9; ++i) {
        scanf("%d", &scores[i][0]);
        score[0] += scores[i][0];
        allscores[k++] = scores[i][0];
        int n = 0, minval = scores[i][0];
        int min[40];
        min[n] = 0;
        for (int j = 1; j < 4; ++j) {
            scanf(", %d", &scores[i][j]);
            allscores[k++] = scores[i][j];
            score[j] += scores[i][j];
            if(scores[i][j] < minval)
                minval = scores[i][j], n = 0, min[0] = j;
            else if(scores[i][j] == minval)
                min[++n] = j;
        }
        if(n == 0)
            wins[min[0]]++;
        scanf("\n");
    }
    if(score[1] - par != 0)
        cout<<abs(score[1] - par)<<' ';
    if(score[1] - par < 0)
        cout<<"under ";
    else if(score[1] - par > 0)
        cout<<"over ";
    cout<<"par\n";
    if(score[0] - par != 0)
        cout<<abs(score[0] - par)<<' ';
    if(score[0] - par < 0)
        cout<<"under ";
    else if(score[0] - par > 0)
        cout<<"over ";
    cout<<"par\n";
    int max = 0, maxval = score[0];
    for (int i = 1; i < 4; ++i) {
        if(score[i] < maxval)
            maxval = score[i], max = i;
    }
    cout<<wins[max]<<'\n';
    pair<char, int> score2[4];
    for (int i = 0; i < 4; ++i) {
        score2[i].first = 'A' + i;
        score2[i].second = score[i];
    }
    sort(score2, score2 + 4, comparer);
    for (int i = 0; i < 4; ++i) {
        cout<<score2[i].first<<' ';
    }
    cout<<"\n";
    if(k % 2 == 0)
        cout<<(allscores[k/2] + allscores[k/2 + 1]) / 2;
    else
        cout<<allscores[k/2 + 1];
    return 0;
}
