#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

ifstream f("components.in");

string convertBits(string bits){
    string result = "";
    for (int i = 0; i < bits.length(); ++i) {
        switch(bits[i]){
            case '0':
                result+="0000";
                break;
            case '1':
                result+="0001";
                break;
            case '2':
                result+="0010";
                break;
            case '3':
                result+="0011";
                break;
            case '4':
                result+="0100";
                break;
            case '5':
                result+="0101";
                break;
            case '6':
                result+="0110";
                break;
            case '7':
                result+="0111";
                break;
            case '8':
                result+="1000";
                break;
            case '9':
                result+="1001";
                break;
            case 'A':
                result+="1010";
                break;
            case 'B':
                result+="1011";
                break;
            case 'C':
                result+="1100";
                break;
            case 'D':
                result+="1101";
                break;
            case 'E':
                result+="1110";
                break;
            case 'F':
                result+="1111";
                break;
        }
    }
    return result;
}

void parcurge(vector<bool> adj[], int i, string &components, int vertices, vector<bool> &used){
    for (int j = 0; j < vertices - i - 1; ++j) {
        if(adj[i][j]){
            adj[i][j] = 0;
            components += 'B' + j + i;
            used[1 + j + i] = 1;
            for(int k = 0; k < j + i + 1; ++k){
                if(adj[k][j + i - k]){
                    adj[k][j + i - k] = 0;
                    used[k] = 1;
                    components += 'A' + k;
                    parcurge(adj, k, components, vertices, used);
                }
            }
            parcurge(adj, j + 1 + i, components, vertices, used);
        }
    }
}

int main()
{
    for(int acsl = 0; acsl < 10; ++acsl){
    vector<string> components;
    vector<bool> adj[26], used;
    string bits;
    int vertices;
    char comma;
    f>>vertices>>comma>>bits;
    bits = convertBits(bits);
    for(int i=0, p=0; i<vertices - 1; ++i){
        used.push_back(0);
        for(int j=0; j<vertices - i - 1; ++j, ++p){
            adj[i].push_back(bits[p] - '0');
        }
    }
    used.push_back(0);
    for (int i = 0; i < vertices; ++i) {
        string comp = "";
        if(!used[i]){
            comp += 'A' + i;
            parcurge(adj, i, comp, vertices, used);
            sort(comp.begin(), comp.end());
            for (int j = 0; j < comp.size() - 1; ++j) {
                if(comp[j] == comp[j + 1]){
                    comp.erase(j, 1);
                    --j;
                }
            }
            components.push_back(comp);
        }
    }
    cout<<components.size()<<" ";
    for (int i = 0; i < components.size(); ++i) {
        if(components[i].find('A') != components[i].npos)
            cout<<components[i]<<"\n";
    }
    }
    return 0;
}
