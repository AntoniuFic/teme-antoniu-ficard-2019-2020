#include <iostream>
#include <cstdio>
#include <algorithm>


using namespace std;

struct draft{
    int years, value, annualSalary;
    bool operator<(draft other){
        return this->annualSalary < other.annualSalary;
    }
}player[10];

int main() {
    freopen("draftpicks.in", "r", stdin);
    int min = 0, max = 0;
    long long salary = 0, salarydif = 0;
    for (int acsl = 0; acsl < 10; ++acsl) {
        double value;
        scanf("%d, %lf\n", &player[acsl].years, &value);
        player[acsl].value = value * 1000000;
        player[acsl].annualSalary = player[acsl].value / player[acsl].years;
        if(player[acsl].annualSalary > player[max].annualSalary)
            max = acsl;
        if(player[acsl].annualSalary < player[min].annualSalary)
            min = acsl;
        salary += 1LL * player[acsl].annualSalary;
        salarydif += 1LL * player[acsl].annualSalary / 16 - 1LL * player[acsl].annualSalary / 18;
    }
    cout<<salary / 10<<"\n"<<player[max].annualSalary<<" by #"<<max + 1<<"\n"<<(player[max].annualSalary - player[min].annualSalary) / 16<<"\n"<<(player[max].annualSalary + player[min].annualSalary) / 36<<"\n"<<salarydif / 10;

    return 0;
}
