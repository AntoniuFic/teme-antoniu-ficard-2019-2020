#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>

using namespace std;

map<char, int> cards;

void assignMap(){
    cards['A'] = 1;
    cards['2'] = 2;
    cards['3'] = 3;
    cards['4'] = 4;
    cards['5'] = 5;
    cards['6'] = 6;
    cards['7'] = 7;
    cards['8'] = 8;
    cards['9'] = 9;
    cards['T'] = 10;
    cards['J'] = 11;
    cards['Q'] = 12;
    cards['K'] = 13;
}

struct card{
    char type, value;
    bool operator<(card other){
        map<char, int> types;
        types['C'] = 0;
        types['D'] = 1;
        types['H'] = 2;
        types['S'] = 3;
        if(cards[this->value] < cards[other.value])
            return true;
        else if(cards[this->value] > cards[other.value])
            return false;
        else return types[this->type] < types[other.type];
    }
};

ifstream f("agram.in");

int main() {
    assignMap();
    for (int acsl = 0; acsl < 5; ++acsl) {
        card oponent{}, dealer[5];
        char comma;
        f>>oponent.value>>oponent.type;
        for (auto & i : dealer) {
            f>>comma>>i.value>>i.type;
        }
        sort(dealer, dealer + 5);
        int result = -1;
        for (int i = 0; i < 5 && result == -1; ++i) {
            if(cards[dealer[i].value] > cards[oponent.value] && dealer[i].type == oponent.type)
                result = i;
        }
        for (int i = 0; i < 5 && result == -1; ++i) {
            if(dealer[i].type == oponent.type)
                result = i;
        }
        if(result == -1)
            result = 0;
        cout<<dealer[result].value<<dealer[result].type<<"\n";
    }
    return 0;
}
