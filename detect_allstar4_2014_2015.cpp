#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

int countBits(int x){
    int i;
    for (i = 0; x; x = (x >> 1))
        if(x & 1)
            ++i;
    return i;
}

int encoded(string mask, int bitstring){
    int result = 0;
    for (int i = mask.length() - 1, j = 0; i >= 0; --i, ++j) {
        result += (mask[i] - '0') * ((bitstring >> j) & 1);
    }
    return result;
}

string bitString(int number, int length){
    string result;
    for(int i = length - 1; i >= 0; --i)
        result.push_back('0' + ((number >> i) & 1));
    return result;
}

int main() {
    ifstream f("detect.in");
    for (int acsl = 0; acsl < 10; ++acsl) {
        int digit, ones;
        vector<string> results;
        string mask;
        getline(f, mask, ',');
        char comma;
        f>>ones>>comma>>digit;
        f.get();
        for (int i = 0; i < (1 << mask.length()) + 1; ++i) {
            if(countBits(i) == ones && encoded(mask, i) == digit)
                results.push_back(bitString(i, mask.length()));
        }
        if(results.size() == 0){
            cout<<"NONE\n";
            continue;
        }
        cout<<results[0];
        for (int i = 1; i < results.size(); ++i) {
            cout<<", "<<results[i];
        }
        cout<<"\n";
    }
    return 0;
}
