#include <cstdio>
#include <iostream>

using namespace std;

int main() {
    freopen("prints.in", "r", stdin);
    for (int acsl = 0; acsl < 5; ++acsl) {
        int even, odd;
        scanf("%d, %d\n", &even, &odd);
        even--;
        odd--;
        bool ok = false;
        for (int i = 0; i < 5; ++i) {
            if((even >> i) & 1){
                if(ok)
                    cout<<", ";
                cout<<2 * (5 - i);
                ok = true;
            }
            if((odd >> i) & 1){
                if(ok)
                    cout<<", ";
                cout<<2 * (5-i) - 1;
                ok = true;
            }
        }
        if(!ok)
            cout<<"None";
        cout<<"\n";
    }
    return 0;
}
